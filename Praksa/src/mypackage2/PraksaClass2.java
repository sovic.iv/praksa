package mypackage2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestListener;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
@Listeners(mypackage2.ListenerTest.class)

public class PraksaClass2 {
    WebDriver driver;
    String url;

    @BeforeClass
    public void init(){
        driver = new ChromeDriver();
        url =  "http://demo.guru99.com/V4/ ";
        driver.get(url);
    }

    @Test
    public void login() {
        driver.findElement(By.name("uid")).sendKeys("mngr34926");
        driver.findElement(By.xpath("/html/body/form/table/tbody/tr[2]/td[2]/input")).sendKeys("amUpenu");
        driver.findElement(By.name("btnLogin")).submit();
    }

    @Test
    public void toFail(){
        System.out.println("This method to test fail");
        Assert.assertTrue(false);
    }
}
