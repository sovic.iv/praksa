package mypackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestGuruPOM {
    //DATUM 15.12.2020
//    WebDriver driver;
//    LoginPOM objLogin;
//    HomePagePOM objHomePage;
//
//    @BeforeTest
//    public void setUp(){
//        driver = new ChromeDriver();
//        driver.get("http://demo.guru99.com/V4/");
//    }
//
//    @Test(priority = 0)
//    public void testHomePageAppearCorrect(){
//        objLogin = new LoginPOM(driver);
//        String loginPageTitle = objLogin.getLoginTitle();
//        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
//
//        objLogin.loginToGuru99("mgr123","mgr!23");
//        objHomePage = new HomePagePOM(driver);
//        Assert.assertTrue(objHomePage.getHomePageUserNmae().toLowerCase().contains("manger id : mgr123"));
//    }
//---------------------------------------------------------------------------------------------------------------------
    WebDriver driver;
    LoginPOM objLogin;
    HomePagePOM objHomePage;

    @BeforeTest
    public void setup(){
        driver = new ChromeDriver();
        driver.get("http://demo.guru99.com/V4/");
    }

    @Test(priority = 0)
    public void testHomePageAppearCorrect(){
        objLogin = new LoginPOM(driver);
        String loginPageTitle = objLogin.getLoginTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));

        objLogin.loginToGuru99("mgr123","mgr!23");

        objHomePage = new HomePagePOM(driver);
        Assert.assertTrue(objHomePage.getHomePageUserName().toLowerCase().contains("manger id : mgr123"));
    }
}
