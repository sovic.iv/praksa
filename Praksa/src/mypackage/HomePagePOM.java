package mypackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePagePOM {
    //DATUM 15.12.2020
//    WebDriver driver;
//    By homePageUserName = By.xpath("//table//tr[@class='heading3']");
//
//    public HomePagePOM(WebDriver driver){
//        this.driver = driver;
//    }
//
//    public String getHomePageUserNmae(){
//        return driver.findElement(homePageUserName).getText();
//    }
//--------------------------------------------------------------------------
    WebDriver driver;
    @FindBy(xpath = "//table//tr[@class='heading3']")
    WebElement homePageUserName;

    public HomePagePOM(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getHomePageUserName(){
        return homePageUserName.getText();
    }
}
