package mypackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPOM {
    //DATUM 15.12.2020
    //    WebDriver driver;
//    By user99GuruName = By.name("uid");
//    By password99Guru = By.name("password");
//    By titleText = By.className("barone");
//    By login = By.name("btnLogin");
//
//    public LoginPOM(WebDriver driver) {
//        this.driver = driver;
//    }
//
//    public void setUserName(String userName) {
//        driver.findElement(user99GuruName).sendKeys(userName);
//    }
//
//    public void setPassword(String password) {
//        driver.findElement(password99Guru).sendKeys(password);
//    }
//
//    public void clickLogin() {
//        driver.findElement(login).click();
//    }
//
//    public String getLoginTitle() {
//        return driver.findElement(titleText).getText();
//    }
//
//    public void loginToGuru99(String userName, String password) {
//        this.setUserName(userName);
//        this.setPassword(password);
//        this.clickLogin();
//    }
//------------------------------------------------------------------------------
    WebDriver driver;
    @FindBy(name = "uid")
    WebElement user99GuruName;

    @FindBy(name = "password")
    WebElement password99Guru;

    @FindBy(className = "barone")
    WebElement titleText;

    @FindBy(name = "btnLogin")
    WebElement login;

    public LoginPOM(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setUser99GuruName(String userName) {
        user99GuruName.sendKeys(userName);
    }

    public void setPassword99Guru(String password) {
        password99Guru.sendKeys(password);
    }

    public void clickLogin() {
        login.click();
    }

    public String getLoginTitle() {
        return titleText.getText();
    }

    public void loginToGuru99(String userName, String password) {
        this.setUser99GuruName(userName);
        this.setPassword99Guru(password);
        this.clickLogin();
    }


}

