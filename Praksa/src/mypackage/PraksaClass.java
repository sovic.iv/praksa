package mypackage;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PraksaClass {
//    public static void main(String[] args) {
//        WebDriver driver = new ChromeDriver();
//        String url = "";

//-----------------------------------------------------------------------
//        url= "http://demo.guru99.com/";
//        driver.get(url);
//        driver.findElement(By.name("emailid")).sendKeys("abc@gmail.com");
//        driver.findElement(By.name("btnLogin")).click();

//-----------------------------------------------------------------------

//        url = "http://demo.guru99.com/test/ajax.html";
//        driver.get(url);
//        WebElement radio1 = driver.findElement(By.id("yes"));
//        WebElement radio2 = driver.findElement(By.id("no"));
//        radio1.click();
//        driver.findElement(By.xpath("/html/body/div[2]/form/p[2]/input[1]")).click(); //reset
//        radio2.click();
//        driver.findElement(By.id("buttoncheck")).click();
//        WebElement text = driver.findElement(By.xpath("/html/body/div[2]/form/p[3]"));
//        System.out.println(text.getText());
//
//        List<WebElement> elements = driver.findElements(By.name("name"));
//        for (int i = 0; i < elements.size(); i++){
//            System.out.println(elements.get(i).getAttribute("value"));
//        }

//------------------------------------------------------------------------
//            url = "http://demo.guru99.com/test/login.html";
//            driver.get(url);
//
//            WebElement email = driver.findElement(By.id("email"));
//            email.sendKeys("abc@gmail.com");
//            email.clear();
//            email.sendKeys("abc@gmail.com");
//            driver.findElement(By.id("passwd")).sendKeys("passwd123");
//            driver.findElement(By.id("SubmitLogin")).submit();
//
//            driver.close();
//-------------------------------------------------------------------------
//            url = "http://demo.guru99.com/test/radio.html";
//            driver.get(url);
//
//            driver.findElement(By.id("vfb-7-2")).click();
//
//            WebElement check1 = driver.findElement(By.id("vfb-6-0"));
//            WebElement check2 = driver.findElement(By.id("vfb-6-2"));
//
//            check1.click();
//            System.out.println("Checkbox1: " + check1.isSelected());
//            System.out.println("Checkbox2: " + check2.isSelected());
//            check2.click();
//            System.out.println("Checkbox2: " + check2.isSelected());
//
//            driver.close();
//---------------------------------------------------------------------------
//        url = "http://demo.guru99.com/test/newtours/register.php";
//        driver.get(url);
//
//        Select dropdown = new Select(driver.findElement(By.name("country")));
//        dropdown.selectByVisibleText("SERBIA");
//
//        driver.close();
//----------------------------------------------------------------------------
//        url = "http://output.jsbin.com/osebed/2";
//        driver.get(url);
//
//        Select fruits = new Select(driver.findElement(By.id("fruits")));
//        fruits.selectByVisibleText("Orange");
//        fruits.selectByIndex(0);
//        fruits.deselectByIndex(0);
//
//        driver.close();
//----------------------------------------------------------------------------
//        url = "http://demo.guru99.com/test/login.html";
//        driver.get(url);
//
//        WebElement email = driver.findElement(By.id("email"));
////      email.sendKeys("abc@gmail.com");
//
//        Actions action = new Actions(driver);
//        Action seriesOfActions = action
//                .moveToElement(email)
//                .click()
//                .keyDown(email, Keys.SHIFT)
//                .sendKeys(email,"abc@gmail.com")
//                .keyUp(email,Keys.SHIFT)
//                .contextClick().build();
//        seriesOfActions.perform();
//
//        driver.findElement(By.id("passwd")).sendKeys("passwd123");
//        driver.findElement(By.id("SubmitLogin")).submit();
//
//        driver.close();
//-----------------------------------------------------------------------------
//        url = "http://demo.guru99.com/test/delete_customer.php";
//        driver.get(url);
//
//        WebElement customerID = driver.findElement(By.name("cusid"));
//        customerID.sendKeys("5");
//        driver.findElement(By.name("submit")).click();
//        driver.switchTo().alert().accept();
//        driver.switchTo().alert().accept();
//
//        driver.close();
//------------------------------------------------------------------------------
//        url = "https://www.testandquiz.com/selenium/testing.html";
//        driver.get(url);
//
//        WebElement textField = driver.findElement(By.id("fname"));
//        textField.sendKeys("Tekst");
//        driver.findElement(By.linkText("This is a link")).click();
//        driver.navigate().back();
//
//        driver.findElement(By.id("idOfButton")).click();
//
//        driver.findElement(By.id("female")).click();
//
//        driver.findElement(By.cssSelector("input.automation")).click();
//
//        Select dropdown = new Select(driver.findElement(By.id("testingDropdown")));
//        dropdown.selectByIndex(1);
//
//        Actions action = new Actions(driver);
//        action.doubleClick(driver.findElement(By.id("dblClkBtn"))).perform();
//        driver.switchTo().alert().accept();
//
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button")).click();
//        driver.switchTo().alert().accept();
//
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        driver.findElement(By.xpath("/html/body/div/div[12]/div/p[1]/button")).click();
//        driver.switchTo().alert().dismiss();
//
//        WebElement tekst = driver.findElement(By.id("demo"));
//        System.out.println(tekst.getText());
//
//        /*WebElement photo = driver.findElement(By.id("sourceImage"));
//        WebElement div = driver.findElement(By.id("targetDiv"));
//
//        Actions action2 = new Actions(driver);
//        action2.dragAndDrop(photo,div).build().perform();*/
// ------------------------------------------------------------------------------
        //DATUM 9.12.2020.

//
//        url = "https://demoqa.com/select-menu";
//        driver.get(url);
//        Select dropdown = new Select(driver.findElement(By.id("cars")));
//        dropdown.selectByValue("saab");
//        dropdown.selectByValue("volvo");
//
//        List<WebElement> options = dropdown.getOptions();
//        WebElement element = dropdown.getFirstSelectedOption();
//        List<WebElement> allSelected = dropdown.getAllSelectedOptions();
//
//        Select oldSelectMenu = new Select(driver.findElement(By.id("oldSelectMenu")));
//        List<WebElement> menu = oldSelectMenu.getOptions();
//
//        oldSelectMenu.selectByIndex(4);
//------------------------------------------------------------------------------------------
//
//        url = "https://demoqa.com/browser-windows";
//        driver.get(url);
//
//        driver.findElement(By.id("windowButton")).click();
//        driver.findElement(By.id("messageWindowButton")).click();
//        driver.findElement(By.id("tabButton")).click();
//
//        String mainWindow = driver.getWindowHandle();
//        Set<String> set = driver.getWindowHandles();
//
//        Iterator<String> iterator = set.iterator();
//
//        while (iterator.hasNext()){
//            String childWindow = iterator.next();
//            if(!mainWindow.equals(childWindow)){
//                driver.switchTo().window(childWindow);
//                driver.close();
//            }
//        }
//        driver.switchTo().window(mainWindow);
//        driver.close();
////-------------------------------------------------------------------------------------
//        url = "https://demoqa.com/alerts";
//        driver.get(url);
//
//        driver.findElement(By.id("alertButton")).click();
//        driver.switchTo().alert().accept();
//
//        driver.findElement(By.id("confirmButton")).click();
//        driver.switchTo().alert().accept();
//
//        driver.findElement(By.id("timerAlertButton")).click();
//        try {
//            WebDriverWait wait = new WebDriverWait(driver,10);
//            wait.until(ExpectedConditions.alertIsPresent());
//
//            Alert alert = driver.switchTo().alert();
//            alert.accept();
//        }
//        catch (NoAlertPresentException e){
//            e.getMessage();
//        }
//        driver.findElement(By.id("promtButton")).click();
//        try {
//            WebDriverWait wait = new WebDriverWait(driver,5);
//            wait.until(ExpectedConditions.alertIsPresent());
//
//            Alert alert = driver.switchTo().alert();
//            System.out.println(alert.getText());  //imam pristup
//           // alert.accept(); //radi
//            alert.sendKeys("Alert");
//        }
//        catch (NoAlertPresentException e){
//            e.getMessage();
//        }
////--------------------------------------------------------------------------------------
//        url = "https://demoqa.com/text-box";
//        driver.get(url);
//
//        Actions action = new Actions(driver);
//
//        driver.findElement(By.id("userName")).sendKeys("Mr.Peter Haynes");
//        driver.findElement(By.id("userEmail")).sendKeys("PeterHaynes@toolsqa.com");
//        WebElement address = driver.findElement(By.id("currentAddress"));
//        address.sendKeys("43 School Lane London EC71 9GO");
//        action.keyDown(Keys.CONTROL);
//        action.sendKeys("a");
//        action.keyUp(Keys.CONTROL);
//        action.build().perform();
//
//       // action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).build().perform();
//
//        action.keyDown(Keys.CONTROL);
//        action.sendKeys("c");
//        action.keyUp(Keys.CONTROL);
//        action.build().perform();
//
//
//        action.sendKeys(Keys.TAB);
//        action.build().perform();
//
//        action.keyDown(Keys.CONTROL).sendKeys("v").keyUp(Keys.CONTROL).build().perform();
//
////        action.keyDown(Keys.CONTROL);
////        action.sendKeys("v");
////        action.keyUp(Keys.CONTROL);
////        action.build().perform();
//
//        driver.findElement(By.id("submit")).submit();
//        driver.close();
//-------------------------------------------------------------------------------------------------
        //DATUM 10.12.2020.
//
//        String homePage = "http://www.zlti.com";
//        driver.get(homePage);
//
//        HttpURLConnection huc = null;
//        int respCode = 200;
//
//        List<WebElement> links = driver.findElements(By.tagName("a"));
//        Iterator<WebElement> it = links.iterator();
//
//        while (it.hasNext()){
//            url = it.next().getAttribute("href");
//            if(url == null || url.isEmpty()){
//                System.out.println("Url null or empty");
//                continue;
//            }
//            if(!(url.startsWith(homePage))){
//                System.out.println(url +" " + "Another domain");
//                continue;
//            }
//            try {
//                huc = (HttpURLConnection)(new URL(url).openConnection());
//                huc.setRequestMethod("HEAD");
//                huc.connect();
//                respCode = huc.getResponseCode();
//
//                if(respCode >= 400){
//                    System.out.println("broken link");
//                }
//                else{
//                    System.out.println("valid link");
//                }
//            }
//            catch (MalformedURLException e){
//                e.printStackTrace();
//            }
//            catch (IOException e){
//                e.printStackTrace();
//            }
//        }
//        driver.quit();
//---------------------------------------------------------------------------------
//        url = "http://only-testing-blog.blogspot.com/2014/01/new-testing.html?";
//        driver.get(url);
//
//        driver.findElement(By.linkText("Click Here")).click();
//        driver.navigate().back();
//
//        Select select = new Select(driver.findElement(By.name("FromLB")));
//        select.selectByValue("Mexico");
//        select.selectByValue("USA");
//
////        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[1]/div[2]/div[1]/form[2]/table/tbody/tr/td[2]/input[1]")).click();
////
////        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[1]/div[2]/div[1]/button[2]")).click();
////        Alert alert = driver.switchTo().alert();
////        alert.sendKeys("Name");
//        //System.out.println(alert.getText());   imam pristup
//
//        String title = "";
//        title = driver.getTitle();
//        // Assert.assertEquals("Message","Naslov",title);
//        // Assert.assertEquals("Message",title,title);
//        //Assert.assertNotEquals("Message","Naslov",title);
//        Assert.assertNotEquals("Message", "Only Testing: New Testing", title);
//
//        driver.close();
//-------------------------------------------------------------------------------------------------

    //}
        // DATUM 11.12.2020.
//    WebDriver driver;
//
//    @Test
//    public void f() {
//        String url = "http://demo.guru99.com/";
//
//        driver = new ChromeDriver();
//        driver.get(url);
//        String testTitle = "Guru99 Bank Home Page";
//        String title = driver.getTitle();
//        Assert.assertEquals(title, testTitle);
//
//    }
//
//    @BeforeMethod
//    public void beforeMethod(){
//        System.out.println("Starting Test on Chrome Browser");
//    }
//
//    @AfterMethod
//    public void afterMethod(){
//        //driver.close();
//        System.out.println("Finished Test on Chrome Browser");
//    }
//-----------------------------------------------------------------------
//    public class TestNG {
//        @Test
//        public void function(){
//            System.out.println("Hello");
//        }
//    }
//-----------------------------------------------------------------------
    //DATUM 14.12.2020.
//
//    @Test(priority = 6)
//    public void c_method(){
//        System.out.println("I'm method C");
//    }
//
//    @Test(priority = 9)
//    public void b_method(){
//        System.out.println("I'm method B");
//    }
//
//    @Test(priority = 6)
//    public void a_method(){
//        System.out.println("I'm method A");
//    }
//
//    @Test(priority = 0)
//    public void e_method(){
//        System.out.println("I'm method E");
//    }
//
//    @Test(priority = 3)
//    public void d_method(){
//        System.out.println("I'm method D");
//    }
//-------------------------------------------------------------------
//    @Test
//    public void c_method() {
//        System.out.println("I'm in method C");
//    }
//
//    @Test
//    public void b_method() {
//        System.out.println("I'm in method B");
//    }
//
//    @Test(priority = 6)
//    public void a_method() {
//        System.out.println("I'm in method A");
//    }
//
//    @Test(priority = 0)
//    public void e_method() {
//        System.out.println("I'm in method E");
//    }
//
//    @Test(priority = 6)
//    public void d_method() {
//        System.out.println("I'm in method D");
//    }
//---------------------------------------------------------------------------
//    WebDriver driver;
//    String url;
//    @Test
//    public void sessionOne(){
//        driver = new ChromeDriver();
//        url = "http://demo.guru99.com/V4/";
//        driver.get(url);
//
//        driver.findElement(By.name("uid")).sendKeys("Driver 1");
//    }
//
//    @Test
//    public void sessionTwo(){
//        driver = new ChromeDriver();
//        url = "http://demo.guru99.com/V4/";
//        driver.get(url);
//
//        driver.findElement(By.name("uid")).sendKeys("Driver 2");
//    }
//
//    @Test
//    public void sessionThree(){
//        driver = new ChromeDriver();
//        url = "http://demo.guru99.com/V4/";
//        driver.get(url);
//
//        driver.findElement(By.name("uid")).sendKeys("Driver 3");
//    }
//---------------------------------------------------------------------------------


}
