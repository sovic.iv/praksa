import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class praksaSelenium {
    public static void main(String[] args){
        WebDriver driver = new ChromeDriver();
        String url;

//-----------------------------------------------------------------------
//        url= "http://demo.guru99.com/";
//        driver.get(url);
//        driver.findElement(By.name("emailid")).sendKeys("abc@gmail.com");
//        driver.findElement(By.name("btnLogin")).click();

//-----------------------------------------------------------------------

//        url = "http://demo.guru99.com/test/ajax.html";
//        driver.get(url);
//        WebElement radio1 = driver.findElement(By.id("yes"));
//        WebElement radio2 = driver.findElement(By.id("no"));
//        radio1.click();
//        driver.findElement(By.xpath("/html/body/div[2]/form/p[2]/input[1]")).click(); //reset
//        radio2.click();
//        driver.findElement(By.id("buttoncheck")).click();
//        WebElement text = driver.findElement(By.xpath("/html/body/div[2]/form/p[3]"));
//        System.out.println(text.getText());
//
//        List<WebElement> elements = driver.findElements(By.name("name"));
//        for (int i = 0; i < elements.size(); i++){
//            System.out.println(elements.get(i).getAttribute("value"));
//        }

//------------------------------------------------------------------------
//            url = "http://demo.guru99.com/test/login.html";
//            driver.get(url);
//
//            WebElement email = driver.findElement(By.id("email"));
//            email.sendKeys("abc@gmail.com");
//            email.clear();
//            email.sendKeys("abc@gmail.com");
//            driver.findElement(By.id("passwd")).sendKeys("passwd123");
//            driver.findElement(By.id("SubmitLogin")).submit();
//
//            driver.close();
//-------------------------------------------------------------------------
//            url = "http://demo.guru99.com/test/radio.html";
//            driver.get(url);
//
//            driver.findElement(By.id("vfb-7-2")).click();
//
//            WebElement check1 = driver.findElement(By.id("vfb-6-0"));
//            WebElement check2 = driver.findElement(By.id("vfb-6-2"));
//
//            check1.click();
//            System.out.println("Checkbox1: " + check1.isSelected());
//            System.out.println("Checkbox2: " + check2.isSelected());
//            check2.click();
//            System.out.println("Checkbox2: " + check2.isSelected());
//
//            driver.close();
//---------------------------------------------------------------------------
//        url = "http://demo.guru99.com/test/newtours/register.php";
//        driver.get(url);
//
//        Select dropdown = new Select(driver.findElement(By.name("country")));
//        dropdown.selectByVisibleText("SERBIA");
//
//        driver.close();
//----------------------------------------------------------------------------
//        url = "http://output.jsbin.com/osebed/2";
//        driver.get(url);
//
//        Select fruits = new Select(driver.findElement(By.id("fruits")));
//        fruits.selectByVisibleText("Orange");
//        fruits.selectByIndex(0);
//        fruits.deselectByIndex(0);
//
//        driver.close();
//----------------------------------------------------------------------------
//        url = "http://demo.guru99.com/test/login.html";
//        driver.get(url);
//
//        WebElement email = driver.findElement(By.id("email"));
////      email.sendKeys("abc@gmail.com");
//
//        Actions action = new Actions(driver);
//        Action seriesOfActions = action
//                .moveToElement(email)
//                .click()
//                .keyDown(email, Keys.SHIFT)
//                .sendKeys(email,"abc@gmail.com")
//                .keyUp(email,Keys.SHIFT)
//                .contextClick().build();
//        seriesOfActions.perform();
//
//        driver.findElement(By.id("passwd")).sendKeys("passwd123");
//        driver.findElement(By.id("SubmitLogin")).submit();
//
//        driver.close();
//-----------------------------------------------------------------------------
//        url = "http://demo.guru99.com/test/delete_customer.php";
//        driver.get(url);
//
//        WebElement customerID = driver.findElement(By.name("cusid"));
//        customerID.sendKeys("5");
//        driver.findElement(By.name("submit")).click();
//        driver.switchTo().alert().accept();
//        driver.switchTo().alert().accept();
//
//        driver.close();
//------------------------------------------------------------------------------
        url = "https://www.testandquiz.com/selenium/testing.html";
        driver.get(url);

        WebElement textField = driver.findElement(By.id("fname"));
        textField.sendKeys("Tekst");
        driver.findElement(By.linkText("This is a link")).click();
        driver.navigate().back();

        driver.findElement(By.id("idOfButton")).click();

        driver.findElement(By.id("female")).click();

        driver.findElement(By.cssSelector("input.automation")).click();

        Select dropdown = new Select(driver.findElement(By.id("testingDropdown")));
        dropdown.selectByIndex(1);

        Actions action = new Actions(driver);
        action.doubleClick(driver.findElement(By.id("dblClkBtn"))).perform();
        driver.switchTo().alert().accept();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button")).click();
        driver.switchTo().alert().accept();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("/html/body/div/div[12]/div/p[1]/button")).click();
        driver.switchTo().alert().dismiss();

        WebElement tekst = driver.findElement(By.id("demo"));
        System.out.println(tekst.getText());

        /*WebElement photo = driver.findElement(By.id("sourceImage"));
        WebElement div = driver.findElement(By.id("targetDiv"));

        Actions action2 = new Actions(driver);
        action2.dragAndDrop(photo,div).build().perform();*/

    }
}
